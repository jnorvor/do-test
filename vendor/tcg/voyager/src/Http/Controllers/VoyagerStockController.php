<?php

namespace TCG\Voyager\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use TCG\Voyager\Facades\Voyager;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Redirect;
use File;
use Excel;
use App\StockTrail;
use DB;

class VoyagerStockController extends Controller
{
    public function index(Request $request)
    {
        // $product = $request->input('product');
        // $new = $request->input('new');
        // $added = $request->input('added');
        $stock = new StockTrail;
        $stock->product_id = $request->input('prod');
        $stock->prev_quantity = $request->input('old');
        $stock->new_quantity = $request->input('new');
        $stock->store_id = $request->input('store');
        $stock->added_quantity = $request->input('old')+$request->input('new');
        $stock->unit_cost_price = $request->input('unit');
        $stock->save();
        return Redirect::back()->with('message', 'Successful Upload');
        // $path = $request->input('name');
		// return back();
    }

    public function modal($prod,$old,$store)
    {
        // $view = "voyager::$slug.edit-add";
        $stock['prod'] = $prod;
        $stock['old']=$old;
        $stores['store']=$store;

        return Voyager::view('voyager::partials.stock',compact('stock','stocks','stores'));
    }


}
