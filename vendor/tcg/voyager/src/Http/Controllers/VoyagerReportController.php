<?php

namespace TCG\Voyager\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use TCG\Voyager\Facades\Voyager;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Redirect;
use File;
use Excel;
use App\Report;
use DB;

class VoyagerReportController extends Controller
{
    public function report(Request $request)
    {
        $report_data = DB::select(DB::raw($request->input('query')));
        $title = $request->input('query_name');
        $headers = array_keys((array)$report_data[0]);
        $headers2 = $headers;
        $actual_data = (array)$report_data;
        for($i=0;$i<count($actual_data);$i++){
            $data[]=(array)$actual_data[$i];
        }
        return Voyager::view('voyager::wureports.browse', compact('data','headers','headers2','title'));
    }

    // public function index(Request $request)
    // {
    //     $report_data = DB::select(DB::raw($request->input('query')));
    //     $title = $request->input('query_name');
    //     $headers = array_keys((array)$report_data[0]);
    //     $headers2 = $headers;
    //     $actual_data = (array)$report_data;
    //     for($i=0;$i<count($actual_data);$i++){
    //         $data[]=(array)$actual_data[$i];
    //     }
    //     return Voyager::view('voyager::wureports.browse', compact('data','headers','headers2','title'));
    // }



}
