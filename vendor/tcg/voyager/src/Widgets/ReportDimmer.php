<?php

namespace TCG\Voyager\Widgets;

use Arrilot\Widgets\AbstractWidget;
use Illuminate\Support\Str;
use TCG\Voyager\Facades\Voyager;
use DB;

class ReportDimmer extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        // $count = Voyager::model('stores')->count();
        $count = DB::table('reports')->count();
        $string = 'Reports';

        return view('voyager::dimmer', array_merge($this->config, [
            'icon'   => 'voyager-bar-chart',
            'title'  => "{$count} {$string}",
            'text'   => __('', ['count' => $count, 'string' => Str::lower($string)]),
            'button' => [
                'text' => __('View All '.$string),
                'link' => route('voyager.reports.index'),
            ],
            // 'image' => voyager_asset('images/widget-backgrounds/01.jpg'),
            'image' => voyager_asset('images/widget-backgrounds/04.jpg'),
        ]));
    }
}
