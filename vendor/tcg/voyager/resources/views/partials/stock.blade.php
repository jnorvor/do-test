@extends('voyager::master')

<!-- <a class="btn btn-danger" id="bulk_delete_btn"><i class="voyager-eye"></i> <span>{{ __('voyager.generic.bulk_delete') }}</span></a> -->
<a class="btn btn" id="stock_btn"><i class="voyager-eye"></i> <span class="hidden-xs hidden-sm">{{ __('voyager.generic.stock') }}</span></a>

{{-- Stock Modal --}}
@section('content')
<!-- <div class="modal modal-danger fade" tabindex="-1" id="bulk_delete_modal" role="dialog"> -->

    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered">
                    <!-- form start -->
                    {{ Form::open(array('route' => 'voyager.add_stock','method' => 'post','files' => true)) }}
                    <input type="hidden" value="{{$stock['prod']}}" name="prod" />
                    <input type="hidden" value="{{$stock['old']}}" name="old" />
                    <input type="hidden" value="{{$stores['store']}}" name="store" />
                    <div style="width:300px; margin:0 auto; text-align: center; padding: 0.5rem;">
                    {{ Form::label('new', 'New Quantity:') }}
                    {{ Form::text('new', null, ['class' => 'uk-input  input-sm form-control', 'placeholder' => 'Enter New Quantity'])}}
                    </div>   
                    <div style="width:300px; margin:0 auto; text-align: center;padding: 0.5rem;">
                    {{ Form::label('unit', 'Unit Cost:') }}
                    {{ Form::text('unit', null, ['class' => 'uk-input input-sm form-control', 'placeholder' => 'Enter Cost Price'])}} 
                    </div>  
                    <div style="text-align: center;padding: 0.5rem;">  
                    <button type="submit" class="form-control" style="width:300px; margin:0 auto; background-color:#FFFFF ! important; color:#d9534f;" value="Submit">{{__('voyager.generic.add_stock')}}</button>   
                    <!-- {{ Form::submit(__('voyager.generic.add_stock') , array('class' => 'btn')) }} -->
                    </div>
                    {{ Form::close() }}

                </div>
            </div>
        </div>
    </div>




<div class="modal modal-danger fade" tabindex="-1" id="stock_modal" role="dialog">    
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">
                   <!-- <i class="voyager-trash"></i>--> {{ __('voyager.generic.stock') }} <span id="bulk_delete_count"></span> <span id="bulk_delete_display_name"></span>
                </h4>
            </div>
            <div class="modal-body" id="bulk_delete_modal_body">
            {{ Form::open(array('route' => 'voyager.add_stock','method' => 'post','files' => true)) }}
            {{ Form::label('product', 'Product:') }}
            {{ Form::text('product', null, ['class' => 'uk-input', 'placeholder' => 'Enter Product'])}}
            {{ Form::label('new', 'New Quantity:') }}
            {{ Form::text('new', null, ['class' => 'uk-input', 'placeholder' => 'Enter New Quantity'])}}            
            {{ Form::label('added', 'Added Quantity:') }}
            {{ Form::text('added', null, ['class' => 'uk-input', 'placeholder' => 'Added Quantity'])}}
            {{ Form::submit(__('voyager.generic.add_stock') , array('class' => 'btn')) }}
            {{ Form::close() }}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-right" data-dismiss="modal">
                    {{ __('voyager.generic.cancel') }}
                </button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@stop
