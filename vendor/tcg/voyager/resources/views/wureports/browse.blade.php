@extends('voyager::master')

@section('page_title', __('voyager.generic.report'))

@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            Reports - {{$title}}
        </h1>
    </div>
@stop
@section('content')
    <div class="page-content browse container-fluid">
        @include('voyager::alerts')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table id="dataTable" class="table table-hover">
                                <thead>
                                    <tr>
                                    @foreach ($headers as $header)
                                        <th>
                                        {{ucwords($header)}}
                                        </th>
                                    @endforeach
                                        <!-- <th class="actions">{{ __('voyager.generic.actions') }}</th> -->
                                    </tr>
                                </thead>
                                <tbody>
                                @for($i=0;$i<count($data);$i++)
                                    <tr>
                                    @for($j=0;$j<count($headers2);$j++)
                                    <td>
                                    {{$data[$i][$headers2[$j]]}}
                                    </td>
                                    @endfor
                                    </tr>
                                @endfor
                                </tbody>
                            </table>
@stop
